---
layout: home
bootstrap: true

# title
postnav_title: "The Zeitgeist Movement"

# second title
postnav_subtitle: "Capitulo México"

# second link
postnav_link: "sobreTZM"

# second linktext
postnav_linktext: "aprende más"

# home page header image
header_image: "/assets/img/autumn-219972_1280.jpg"

---
